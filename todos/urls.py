from django.contrib import admin
from django.urls import path, include

from todos.views import (
    ToDoListView,
)

urlpatterns = [
    path("", ToDoListView.as_view(), name="todo_list_list")
]
