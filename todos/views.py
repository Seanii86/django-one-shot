from django.shortcuts import render
from django.views.generic.list import ListView
from  .models import TodoList
# Create your views here.


class ToDoListView(ListView, TodoList):
    model = TodoList
    template_name = 'todos/list.html'
    paginate_by = 2
